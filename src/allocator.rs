pub mod dummy;
pub mod bump;
pub mod linked_list;
pub mod fixed_size_block;

use x86_64::{
  VirtAddr,
  structures::paging::{mapper::MapToError, FrameAllocator, Mapper, Page, PageTableFlags, Size4KiB},
};

// wrapper around spin::Mutex to permit trait implementations
pub struct Locked<T> {
  inner: spin::Mutex<T>,
}

impl <T> Locked<T> {
  pub const fn new(inner: T) -> Self {
    Locked {
      inner: spin::Mutex::new(inner)
    }
  }

  pub fn lock(&self) -> spin::MutexGuard<T> {
    self.inner.lock()
  }
}

fn align_up(addr: usize, align: usize) -> usize {
  (addr + align - 1) / align * align
}

use dummy::Dummy;
//use linked_list_allocator::LockedHeap;
use bump::BumpAllocator;
use linked_list::LinkedListAllocator;
use fixed_size_block::FixedSizeBlockAllocator;


#[global_allocator]
//static ALLOCATOR: LockedHeap = LockedHeap::empty();
//static ALLOCATOR: Locked<BumpAllocator> = Locked::new(BumpAllocator::new());
//static ALLOCATOR: Locked<LinkedListAllocator> = Locked::new(LinkedListAllocator::new());
static ALLOCATOR: Locked<FixedSizeBlockAllocator> = Locked::new(FixedSizeBlockAllocator::new());

pub fn init_heap(mapper: &mut impl Mapper<Size4KiB>, frame_allocator: &mut impl FrameAllocator<Size4KiB>)
  -> Result<(), MapToError<Size4KiB> >
{
  let page_range = {
    let heap_start = VirtAddr::new(0x_4444_4444_0000 as u64);
    let heap_end = heap_start + (100 * 1024 - 1u64);
    let heap_start_page = Page::containing_address(heap_start);
    let heap_end_page = Page::containing_address(heap_end);
    Page::range_inclusive(heap_start_page, heap_end_page)
  };

  for page in page_range {
    let frame = frame_allocator.allocate_frame().ok_or(MapToError::FrameAllocationFailed)?;
    let flags = PageTableFlags::PRESENT | PageTableFlags::WRITABLE;
    unsafe {
      mapper.map_to(page, frame, flags, frame_allocator)?.flush();
    };
  }

  unsafe {
    ALLOCATOR.lock().init(0x_4444_4444_0000, 100 * 1024);
  }

  Ok(())
}
