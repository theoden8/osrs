#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(osrs::test_runner)]
#![reexport_test_harness_main = "test_main"]

use bootloader::{BootInfo, entry_point};

use core::panic::PanicInfo;
use osrs::println;

extern crate alloc;
use alloc::{boxed::Box, vec, vec::Vec, rc::Rc};

entry_point!(kernel_main);

async fn async_number() -> u32 {
  42
}

async fn example_task() {
  let number = async_number().await;
  println!("async number: {}", number);
}

fn kernel_main(boot_info: &'static BootInfo) -> ! {
  use osrs::{memory, allocator, task};

  println!("Hello World{}", "!");

  osrs::init();

  // trigger a page fault
//  unsafe {
//    *(0xdeadbeef as *mut u64) = 42;
//  };

  let phy_mem_offt = x86_64::VirtAddr::new(boot_info.physical_memory_offset);
  let mut mapper = unsafe { memory::init(phy_mem_offt) };
  let mut frame_allocator = unsafe {
    memory::BootInfoFrameAllocator::init(&boot_info.memory_map)
  };

//  let page_ptr: *mut u64 = page.start_address().as_mut_ptr();
//  unsafe { page_ptr.offset(400).write_volatile(0x_f021_f077_f065_f04e) };

  allocator::init_heap(&mut mapper, &mut frame_allocator).expect("heap init failed");

  let x = Box::new(41);
  println!("heap value at {:p}", x);

  let mut vec = Vec::new();
  for i in 0..500 {
    vec.push(i);
  }
  println!("vec at {:p}", vec.as_slice());

  // create a reference counted vector -> will be freed when count reaches 0
  let reference_counted = Rc::new(vec![1, 2, 3]);
  let cloned_reference = reference_counted.clone();
  println!("current reference count is {}", Rc::strong_count(&cloned_reference));
  core::mem::drop(reference_counted);
  println!("reference count is {} now", Rc::strong_count(&cloned_reference));

//  let addresses = [
//    // vga buffer
//    0xb8000,
//    // some code page
//    0x201008,
//    // some stack page
//    0x0100_0020_1a10,
//    // something mapped to 0
//    boot_info.physical_memory_offset,
//  ];
//  for &address in &addresses {
//    let virt = x86_64::VirtAddr::new(address);
//    let phys = mapper.translate_addr(virt);
//    println!("{:?} -> {:?}", virt, phys);
//  }

  #[cfg(test)]
  test_main();

//let mut executor = task::simple_executor::SimpleExecutor::new();
  let mut executor = task::executor::Executor::new();
  executor.spawn(task::Task::new(example_task()));
  executor.spawn(task::Task::new(task::keyboard::print_keypresses()));
  executor.run();
}

// our existing panic handler
#[cfg(not(test))] // new attribute
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
  println!("{}", info);
  osrs::hlt_loop();
}

// our panic handler in test mode
#[cfg(test)]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
  osrs::test_panic_handler(info);
}

#[test_case]
fn trivial_assertion() {
  assert_eq!(1, 1);
}
