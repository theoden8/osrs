use alloc;

use super::Task;

pub struct SimpleExecutor {
  task_queue: alloc::collections::VecDeque<Task>,
}

fn dummy_raw_waker() -> core::task::RawWaker {
  fn no_op(_: *const ()) {}
  fn clone(_: *const ()) -> core::task::RawWaker {
    dummy_raw_waker()
  }

  let vtable = &core::task::RawWakerVTable::new(clone, no_op, no_op, no_op);
  core::task::RawWaker::new(0 as *const (), vtable)
}

fn dummy_waker() -> core::task::Waker {
  unsafe {
    core::task::Waker::from_raw(dummy_raw_waker())
  }
}

impl SimpleExecutor {
  pub fn new() -> SimpleExecutor {
    SimpleExecutor {
      task_queue: alloc::collections::VecDeque::new(),
    }
  }

  pub fn spawn(&mut self, task: super::Task) {
    self.task_queue.push_back(task)
  }

  pub fn run(&mut self) {
    while let Some(mut task) = self.task_queue.pop_front() {
      let waker = dummy_waker();
      let mut context = core::task::Context::from_waker(&waker);
      match task.poll(&mut context) {
        core::task::Poll::Ready(()) => {},
        core::task::Poll::Pending => self.task_queue.push_back(task)
      }
    }
  }
}

