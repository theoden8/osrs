use conquer_once;
use crossbeam_queue;
use futures_util;
use futures_util::stream::Stream;
use futures_util::stream::StreamExt;
use pc_keyboard;

use crate::{print, println};

// Instead of the OnceCell primitive, we could also use the lazy_static macro here. However, the OnceCell type has the advantage that we can ensure that the initialization does not happen in the interrupt handler, thus preventing the interrupt handler from performing a heap allocation.
static SCANCODE_QUEUE: conquer_once::spin::OnceCell<crossbeam_queue::ArrayQueue<u8>> = conquer_once::spin::OnceCell::uninit();
static WAKER: futures_util::task::AtomicWaker = futures_util::task::AtomicWaker::new();

// Since this function should not be callable from our main.rs, we use the pub(crate) visibility to make it only available to our lib.rs.
pub(crate) fn add_scancode(scancode: u8) {
  if let Ok(queue) = SCANCODE_QUEUE.try_get() {
    if let Err(_) = queue.push(scancode) {
      println!("WARNING: scancode queue full; dropping keyboard input");
    } else {
      WAKER.wake();
    }
  } else {
    println!("WARNING: scancode queue uninitialized");
  }
}

pub struct ScancodeStream {
  _private: (),
}

impl ScancodeStream {
  pub fn new() -> Self {
    SCANCODE_QUEUE.try_init_once(|| crossbeam_queue::ArrayQueue::new(100))
      .expect("ScancodeStream::new should only be called once");
    ScancodeStream { _private: () }
  }
}

impl Stream for ScancodeStream {
  type Item = u8;

  fn poll_next(self: core::pin::Pin<&mut Self>, cx: &mut core::task::Context) -> core::task::Poll<Option<u8>> {
    let queue = SCANCODE_QUEUE
      .try_get()
      .expect("not initialized");

    if let Some(scancode) = queue.pop() {
      return core::task::Poll::Ready(Some(scancode));
    }

    WAKER.register(&cx.waker());

    match queue.pop() {
      Some(scancode) => {
        WAKER.take();
        core::task::Poll::Ready(Some(scancode))
      }
      None => core::task::Poll::Pending,
    }
  }
}

pub async fn print_keypresses() {
    let mut scancodes = ScancodeStream::new();
    let mut keyboard = pc_keyboard::Keyboard::new(pc_keyboard::layouts::Us104Key, pc_keyboard::ScancodeSet1, pc_keyboard::HandleControl::Ignore);

    while let Some(scancode) = scancodes.next().await {
        if let Ok(Some(key_event)) = keyboard.add_byte(scancode) {
            if let Some(key) = keyboard.process_keyevent(key_event) {
                match key {
                    pc_keyboard::DecodedKey::Unicode(character) => print!("{}", character),
                    pc_keyboard::DecodedKey::RawKey(key) => print!("{:?}", key),
                }
            }
        }
    }
}

