use core;
use alloc;

pub mod simple_executor;
pub mod executor;
pub mod keyboard;

pub struct Task {
  id: TaskId,
  future: core::pin::Pin<alloc::boxed::Box<dyn core::future::Future<Output = ()>>>,
}

impl Task {
  pub fn new(future: impl core::future::Future<Output = ()> + 'static) -> Task {
    Task {
      id: TaskId::new(),
      future: alloc::boxed::Box::pin(future),
    }
  }

  fn poll(&mut self, context: &mut core::task::Context) -> core::task::Poll<()> {
    self.future.as_mut().poll(context)
  }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
struct TaskId(u64);

impl TaskId {
    fn new() -> Self {
        static NEXT_ID: core::sync::atomic::AtomicU64 = core::sync::atomic::AtomicU64::new(0);
        TaskId(NEXT_ID.fetch_add(1, core::sync::atomic::Ordering::Relaxed))
    }
}
