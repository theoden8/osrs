use alloc;
use crossbeam_queue;

use super::{Task, TaskId};

struct TaskWaker {
  task_id: TaskId,
  task_queue: alloc::sync::Arc<crossbeam_queue::ArrayQueue<TaskId>>,
}

impl TaskWaker {
  fn new(task_id: TaskId, task_queue: alloc::sync::Arc<crossbeam_queue::ArrayQueue<TaskId>>) -> core::task::Waker {
    core::task::Waker::from(alloc::sync::Arc::new(TaskWaker {
      task_id,
      task_queue,
    }))
  }

  fn wake_task(&self) {
    self.task_queue.push(self.task_id).expect("task_queue full");
  }
}

impl alloc::task::Wake for TaskWaker {
  fn wake(self: alloc::sync::Arc<Self>) {
    self.wake_task();
  }

  fn wake_by_ref(self: &alloc::sync::Arc<Self>) {
    self.wake_task();
  }
}

pub struct Executor {
  tasks: alloc::collections::BTreeMap<TaskId, Task>,
  task_queue: alloc::sync::Arc<crossbeam_queue::ArrayQueue<TaskId>>,
  waker_cache: alloc::collections::BTreeMap<TaskId, core::task::Waker>,
}

impl Executor {
  pub fn new() -> Self {
    Executor {
      tasks: alloc::collections::BTreeMap::new(),
      task_queue: alloc::sync::Arc::new(crossbeam_queue::ArrayQueue::new(100)),
      waker_cache: alloc::collections::BTreeMap::new(),
    }
  }

  pub fn spawn(&mut self, task: Task) {
    let task_id = task.id;
    if self.tasks.insert(task.id, task).is_some() {
      panic!("task with same ID already in tasks");
    }
    self.task_queue.push(task_id).expect("queue full");
  }

  fn run_ready_tasks(&mut self) {
    // destructure `self` to avoid borrow checker errors
    let Self {
      tasks,
      task_queue,
      waker_cache
    } = self;

    while let Some(task_id) = task_queue.pop() {
      let task = match tasks.get_mut(&task_id) {
        Some(task) => task,
        None => continue,
      };
      let waker = waker_cache
        .entry(task_id)
        .or_insert_with(|| TaskWaker::new(task_id, task_queue.clone()));
      let mut context = core::task::Context::from_waker(waker);
      match task.poll(&mut context) {
        core::task::Poll::Ready(()) => {
          tasks.remove(&task_id);
          waker_cache.remove(&task_id);
        }
        core::task::Poll::Pending => {}
      }
    }
  }

  pub fn run(&mut self) -> ! {
    loop {
      self.run_ready_tasks();
      self.sleep_if_idle();
    }
  }

  fn sleep_if_idle(&self) {
    x86_64::instructions::interrupts::disable();
    if self.task_queue.is_empty() {
      x86_64::instructions::interrupts::enable_and_hlt();
    } else {
      x86_64::instructions::interrupts::enable();
    }
  }
}
